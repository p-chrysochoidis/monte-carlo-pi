import { WorkGenerationContract, WorkResultHandler, WebBasedParallelizer, Logger, LogLevel } from "web-based-parallelizer-server";
import { PiWorkGenerator } from "./src/PiWorkGenerator";
import { PiWorkResultHandler } from "./src/PiWorkResultHandler";
import webWorkerCode from "./src/WebWorkerCode";

Logger.gi().setLevel(LogLevel.ALL);
let workerGenerationContract: WorkGenerationContract = new PiWorkGenerator(10000000000, 100000000);
let workResultHandler: WorkResultHandler = new PiWorkResultHandler();
let aServer = new WebBasedParallelizer(webWorkerCode, workerGenerationContract, workResultHandler);
aServer.start();
let sin = process.stdin;
sin.setEncoding('utf-8');
sin.on('data', (key) => {
    key = key.trim();
    console.log('Stdin: ', key);
    if (key === 'start') {
        console.log('Starting work');
        aServer.startWork();
    }
})