import { WorkResultHandler, Work, WorkResult } from "web-based-parallelizer-server";
import { PiWorkResult } from "./PiWorkResult";

export class PiWorkResultHandler extends WorkResultHandler {
    public allCompletedHandleResults(workResults: PiWorkResult[]): void {
        this.logger.info(`All work completed. Total results: ${workResults.length}.`);
        let pi: number = 0;
        let totalRounds: number = 0; // XXX: Number.MAX_SAFE_INTEGER see BigInt 
        let totalScore: number = 0;
        workResults.forEach((result: PiWorkResult) => {
            totalRounds += result.rounds;
            totalScore += result.score;
        });
        pi = this.calculatePi(totalRounds, totalScore);
        this.logger.info(`Computed pi is '${pi}'`);
    }

    public desirializeWorkResult(workResultJson: any, work: Work): WorkResult {
        return new PiWorkResult(work, workResultJson.score, workResultJson.rounds);
    }

    private calculatePi(rounds: number, score: number) {
        return 4 * score/rounds;
    }
}