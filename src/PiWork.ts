import { Work } from "web-based-parallelizer-server";

export class PiWork extends Work {
    constructor(id: string, private rounds: number) {
        super(id);
    }

    toJson() {
        return { rounds: this.rounds };
    }
}