import { WorkResult, Work } from "web-based-parallelizer-server";

export class PiWorkResult extends WorkResult {
    private mScore: number;
    private mRounds: number;

    public get score(): number {
        return this.mScore;
    }

    public get rounds(): number {
        return this.mRounds
    }

    constructor(work: Work, score: number, rounds: number) {
        super(work);
        this.mScore = score;
        this.mRounds = rounds;
    }
}