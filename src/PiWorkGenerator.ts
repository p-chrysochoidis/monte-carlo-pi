import { WorkGenerationContract, Logger, WorkGenerator, WorkResultHandler } from "web-based-parallelizer-server";
import { PiWork } from "./PiWork";

export class PiWorkGenerator implements WorkGenerationContract {
    private logger: Logger = Logger.gi();
    private totalChunksNeeded: number;
    private extraWorkerChunkSize: number;
    private mWorkGenerator: WorkGenerator;

    constructor(private totalSamples: number, private chunkSize: number) {
        this.logger.debug(`[PiWorkGeneratorContract] Total samples: ${this.totalSamples}, chunkSize: ${this.chunkSize}`);
        let wholeChunksNeeded = Math.floor(this.totalSamples / this.chunkSize);
        this.extraWorkerChunkSize = this.totalSamples % this.chunkSize;
        this.totalChunksNeeded = wholeChunksNeeded + (this.extraWorkerChunkSize ? 1 : 0);
        this.logger.debug(`[PiWorkGeneratorContract] Needs ${this.totalChunksNeeded} chunks. Whole chunks: ${wholeChunksNeeded}. Extra worker chunk size: ${this.extraWorkerChunkSize}`);
    }

    public attachWorkGenerator(workGenerator: WorkGenerator): void {
        this.mWorkGenerator = workGenerator;
    }

    public generateWork(): void {
        this.logger.debug(`[PiWorkGeneratorContract] Generating work for workers.`);
        let index = 0;
        new Array(this.totalChunksNeeded).fill(0).forEach(() => {
            let isExtraChunk = this.extraWorkerChunkSize && index === this.totalChunksNeeded - 1;
            if (isExtraChunk) {
                this.logger.debug(`[PiWorkGeneratorContract] Generating extra work chunk`);
            }
            this.mWorkGenerator.submitWork(new PiWork(`${index++}`, isExtraChunk ? this.extraWorkerChunkSize : this.chunkSize));
        });
        this.mWorkGenerator.allWorkGenerationCompleted();
    }

    public handleCompletedWorkResults(workResults: WorkResultHandler): boolean {
        this.logger.debug(`[PiWorkGeneratorContract] handleCompletedWorkResults - do nothing`);
        //Do nothing
        return false;
    }
}