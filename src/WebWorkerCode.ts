export default `
onmessage = function(workEvent) {
    var work = workEvent.data,
        totalRounds = work.rounds,
        rounds = totalRounds,
        score = 0;

    while(rounds--) {
        if(isInCicle(generateCordinates())){
            score++;
        }
    }

    postMessage({
        data: {
            score: score,
            rounds: totalRounds
        },
        work: work
    });
}

function generateCordinates() {
    var x = Math.random(),
        y = Math.random();
    return {x: x, y: y};
}

function isInCicle(point) {
    return (Math.sqrt(Math.pow(point.x, 2) + Math.pow(point.y, 2))) <= 1;
}
`;